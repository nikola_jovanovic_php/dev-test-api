<?php

namespace App;

use Awobaz\Compoships\Database\Eloquent\Model;

/**
 * Class Doctor
 * @package App
 *
 * @property int                                                         $id
 * @property int                                                         $secondary_id
 * @property int                                                         $clinic_id
 * @property string                                                      $name
 * @property \App\Clinic                                                 $clinic
 * @property \Illuminate\Database\Eloquent\Collection|\App\Appointment[] $appointments
 */
class Doctor extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'secondary_id',
        'clinic_id'
    ];

    /**
     * @return \Awobaz\Compoships\Database\Eloquent\Relations\BelongsTo
     */
    public function clinic()
    {
        return $this->belongsTo(Clinic::class, 'clinic_id', 'secondary_id');
    }

    /**
     * @return \Awobaz\Compoships\Database\Eloquent\Relations\HasMany
     */
    public function appointments()
    {
        return $this->hasMany(Appointment::class, ['doctor_id', 'clinic_id'], ['secondary_id', 'clinic_id']);
    }
}
