<?php

namespace App;

use Awobaz\Compoships\Database\Eloquent\Model;

/**
 * Class Patient
 * @package App
 *
 * @property int                                                         $id
 * @property int                                                         $secondary_id
 * @property int                                                         $clinic_id
 * @property string                                                      $name
 * @property \Carbon\Carbon                                              $date_of_birth
 * @property string                                                      $gender
 * @property \App\Clinic                                                 $clinic
 * @property \Illuminate\Database\Eloquent\Collection|\App\Appointment[] $appointments
 */
class Patient extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'date_of_birth',
        'gender',
        'secondary_id',
        'clinic_id',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'date_of_birth',
    ];

    /**
     * @return \Awobaz\Compoships\Database\Eloquent\Relations\BelongsTo
     */
    public function clinic()
    {
        return $this->belongsTo(Clinic::class, 'clinic_id', 'secondary_id');
    }

    /**
     * @return \Awobaz\Compoships\Database\Eloquent\Relations\HasMany
     */
    public function appointments()
    {
        return $this->hasMany(Appointment::class, ['patient_id', 'clinic_id'], ['secondary_id', 'clinic_id']);
    }
}
