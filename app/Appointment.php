<?php

namespace App;

use Awobaz\Compoships\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class Appointment
 * @package App
 *
 * @property int            $id
 * @property int            $secondary_id
 * @property int            $clinic_id
 * @property int            $patient_id
 * @property int            $specialty_id
 * @property int            $doctor_id
 * @property string         $status
 * @property \Carbon\Carbon $booked_at
 * @property \Carbon\Carbon $datetime
 * @property \App\Specialty $specialty
 * @property \App\Doctor    $doctor
 * @property \App\Patient   $patient
 * @property \App\Clinic    $clinic
 * @property string         $state
 * @property string         $startTime
 * @property string         $startDate
 *
 * @method static Builder whereHas($relation, $callback)
 * @method static Builder notCancelled()
 */
class Appointment extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'status',
        'booked_at',
        'datetime',
        'specialty_id',
        'doctor_id',
        'patient_id',
        'secondary_id',
        'clinic_id',
        'specialty_id',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'booked_at',
        'datetime',
    ];

    /**
     * @return string
     */
    public function getStateAttribute()
    {
        return $this->datetime->lt(Carbon::now()) ? 'passed' : 'upcoming';
    }

    /**
     * @return string
     */
    public function getStartDateAttribute()
    {
        return $this->datetime->toDateString();
    }

    /**
     * @return string
     */
    public function getStartTimeAttribute()
    {
        return $this->datetime->toTimeString();
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     */
    public function scopeNotCancelled(Builder $query)
    {
        $query->where('status', '=', 'booked');
    }

    /**
     * @return \Awobaz\Compoships\Database\Eloquent\Relations\BelongsTo
     */
    public function clinic()
    {
        return $this->belongsTo(Clinic::class, 'clinic_id', 'secondary_id');
    }

    /**
     * @return \Awobaz\Compoships\Database\Eloquent\Relations\BelongsTo
     */
    public function specialty()
    {
        return $this->belongsTo(Specialty::class, ['specialty_id', 'clinic_id'], ['secondary_id', 'clinic_id']);
    }

    /**
     * @return \Awobaz\Compoships\Database\Eloquent\Relations\BelongsTo
     */
    public function doctor()
    {
        return $this->belongsTo(Doctor::class, ['doctor_id', 'clinic_id'], ['secondary_id', 'clinic_id']);
    }

    /**
     * @return \Awobaz\Compoships\Database\Eloquent\Relations\BelongsTo
     */
    public function patient()
    {
        return $this->belongsTo(Patient::class, ['patient_id', 'clinic_id'], ['secondary_id', 'clinic_id']);
    }
}
