<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Clinic
 * @package App
 *
 * @property int                                                         $id
 * @property int                                                         $secondary_id
 * @property int                                                         $name
 * @property \Illuminate\Database\Eloquent\Collection|\App\Doctor[]      $doctors
 * @property \Illuminate\Database\Eloquent\Collection|\App\Specialty[]   $specialties
 * @property \Illuminate\Database\Eloquent\Collection|\App\Appointment[] $appointments
 */
class Clinic extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'secondary_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function doctors()
    {
        return $this->hasMany(Doctor::class, 'clinic_id', 'secondary_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function specialties()
    {
        return $this->hasMany(Specialty::class, 'clinic_id', 'secondary_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function appointments()
    {
        return $this->hasMany(Appointment::class, 'clinic_id', 'secondary_id');
    }
}
