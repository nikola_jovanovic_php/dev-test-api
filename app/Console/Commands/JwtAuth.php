<?php

namespace App\Console\Commands;

use App\Console\Commands\Contracts\Mapper;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

/**
 * Class JwtAuth
 * @package App\Console\Commands
 */
class JwtAuth extends Command
{
    use InsertsAppointment;

    /**
     * @var \GuzzleHttp\Client
     */
    private $client;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth:jwt {username} {password} {auth} {url} {mapper} {root_element}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'JWT auth';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->client = new Client();
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $username = explode(':', $this->argument('username'));
        $password = explode(':', $this->argument('password'));
        $auth     = $this->argument('auth');
        $url      = $this->argument('url');
        $mapper   = $this->argument('mapper');
        $root     = $this->argument('root_element');
        if (!in_array(Mapper::class, class_implements($mapper))) {
            throw new \InvalidArgumentException("{$mapper} doesnt imeplemnt interface " . Mapper::class);
        }

        $token = $this->getToken($username, $password, $auth);

        $nextPage = null;
        do {
            $response     = json_decode(
                $this->client->get(
                    $nextPage ?? $url,
                    [
                        'headers' => [
                            'Authorization' => 'Bearer ' . $token,
                        ],
                    ]
                )->getBody()->getContents(),
                true
            );
            $appointments = $response[$root];

            foreach ($appointments as $appointment) {
                $data = call_user_func([$mapper, 'map'], $appointment);
                if (Carbon::now()->diffInYears($data['patient']['date_of_birth']) > 17 ||
                    Carbon::now()->addDays(30)->lt($data['appointment']['datetime']) ||
                    Carbon::now()->gt($data['appointment']['datetime'])) {
                    continue;
                }
                $this->insertAppointment($data);
            }
        } while ($nextPage = $response['next_page_url']);
    }

    /**
     * @param array  $username
     * @param array  $password
     * @param string $auth
     *
     * @return mixed
     */
    private function getToken(array $username, array $password, string $auth)
    {
        return json_decode(
            $this->client->post(
                $auth,
                [
                    'query' => [
                        $username[0] => $username[1],
                        $password[0] => $password[1],
                    ],
                ]
            )->getBody(),
            true
        )['token'];
    }
}
