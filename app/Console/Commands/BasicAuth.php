<?php

namespace App\Console\Commands;

use App\Console\Commands\Contracts\Mapper;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

/**
 * Class BasicAuth
 * @package App\Console\Commands
 */
class BasicAuth extends Command
{
    use InsertsAppointment;

    /**
     * @var \GuzzleHttp\Client
     */
    private $client;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth:basic {username} {password} {url} {mapper} {root_element}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Basic auth';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->client = new Client();
        parent::__construct();
    }

    /**
     * @return string
     */
    public function handle()
    {
        $username = $this->argument('username');
        $password = $this->argument('password');
        $url      = $this->argument('url');
        $mapper   = $this->argument('mapper');
        $root     = $this->argument('root_element');
        if (!in_array(Mapper::class, class_implements($mapper))) {
            throw new \InvalidArgumentException("{$mapper} doesnt imeplemnt interface " . Mapper::class);
        }

        $response     = $this->client->get($url, ['auth' => [$username, $password]])->getBody()->getContents();
        $appointments = simplexml_load_string($response, 'SimpleXMLElement', LIBXML_NOBLANKS | LIBXML_BIGLINES);
        $appointments = json_encode($appointments);
        $appointments = json_decode($appointments, true)[$root];
        foreach ($appointments as $appointment) {
            $data = call_user_func([$mapper, 'map'], $appointment);
            if (Carbon::now()->diffInYears($data['patient']['date_of_birth']) > 17 ||
                Carbon::now()->addDays(30)->lt($data['appointment']['datetime']) ||
                Carbon::now()->gt($data['appointment']['datetime'])) {
                continue;
            }
            $this->insertAppointment($data);
        }
    }
}
