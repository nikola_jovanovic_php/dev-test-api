<?php

namespace App\Console\Commands;

use App\Appointment;
use App\Clinic;
use App\Doctor;
use App\Patient;
use App\Specialty;

/**
 * Trait InsertsAppointment
 * @package App\Console\Commands
 */
trait InsertsAppointment
{
    /**
     * @param array $data
     */
    public function insertAppointment(array $data)
    {
        Clinic::updateOrCreate(
            ['secondary_id' => $data['clinic']['secondary_id']],
            $data['clinic']
        );
        Doctor::updateOrCreate(
            [
                'secondary_id' => $data['doctor']['secondary_id'],
                'clinic_id'    => $data['clinic']['secondary_id'],
            ],
            $data['doctor']
        );
        //            dd($appointment->specialty->name);
        Specialty::updateOrCreate(
            [
                'secondary_id' => $data['specialty']['secondary_id'],
                'clinic_id'    => $data['clinic']['secondary_id'],
            ],
            $data['specialty']
        );
        Patient::updateOrCreate(
            [
                'secondary_id' => $data['patient']['secondary_id'],
                'clinic_id'    => $data['clinic']['secondary_id'],
            ],
            $data['patient']
        );
        Appointment::updateOrCreate(
            [
                'secondary_id' => $data['appointment']['secondary_id'],
                'clinic_id'    => $data['clinic']['secondary_id'],
            ],
            $data['appointment']
        );
    }
}
