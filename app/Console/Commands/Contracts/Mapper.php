<?php

namespace App\Console\Commands\Contracts;

/**
 * Interface Mapper
 * @package App\Console\Commands\Contracts
 */
interface Mapper
{
    /**
     * @param array $data
     *
     * @return array
     */
    public static function map(array $data): array;
}
