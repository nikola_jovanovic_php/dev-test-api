<?php

namespace App\Console\Commands;

use App\Console\Commands\Contracts\Mapper;
use Carbon\Carbon;

/**
 * Class Clinic1Mapper
 * @package App\Console\Commands
 */
class Clinic1Mapper implements Mapper
{
    /**
     * @param array $data
     *
     * @return array
     */
    public static function map(array $data): array
    {
        return [
            'appointment' => [
                'secondary_id' => $data['id'],
                'clinic_id'    => $data['clinic']['id'],
                'status'       => empty($data['cancelled']) ? 'cancelled' : 'booked',
                'booked_at'    => $data['booked_at'],
                'datetime'     => Carbon::createFromFormat(
                    'm/d/Y g:i:s A',
                    $data['start_date'] . ' ' . $data['start_time']
                ),
                'specialty_id' => $data['specialty']['id'],
                'doctor_id'    => $data['doctor']['id'],
                'patient_id'   => $data['patient']['id'],

            ],
            'clinic'      => [
                'secondary_id' => $data['clinic']['id'],
                'name'         => $data['clinic']['name'],
            ],
            'patient'     => [
                'secondary_id'  => $data['patient']['id'],
                'name'          => $data['patient']['name'],
                'gender'        => $data['patient']['sex'] == 1 ? 'male' : 'female',
                'date_of_birth' => Carbon::createFromFormat('m/d/Y', $data['patient']['date_of_birth']),
                'clinic_id'     => $data['clinic']['id'],
            ],
            'doctor'      => [
                'secondary_id' => $data['doctor']['id'],
                'name'         => $data['doctor']['name'],
                'clinic_id'    => $data['clinic']['id'],
            ],
            'specialty'   => [
                'secondary_id' => $data['specialty']['id'],
                'name'         => $data['specialty']['name'],
                'clinic_id'    => $data['clinic']['id'],
            ],
        ];
    }
}
