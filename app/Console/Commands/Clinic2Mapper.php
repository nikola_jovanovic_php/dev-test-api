<?php

namespace App\Console\Commands;

use App\Console\Commands\Contracts\Mapper;
use Carbon\Carbon;

/**
 * Class Clinic2Mapper
 * @package App\Console\Commands
 */
class Clinic2Mapper implements Mapper
{
    /**
     * @param array $data
     *
     * @return array
     */
    public static function map(array $data): array
    {
        return [
            'appointment' => [
                'secondary_id' => $data['id'],
                'clinic_id'    => $data['clinic']['id'],
                'status'       => $data['status'],
                'booked_at'    => $data['created_at'],
                'datetime'     => $data['datetime'],
                'specialty_id' => $data['specialty']['id'],
                'doctor_id'    => $data['doctor']['id'],
                'patient_id'   => $data['patient']['id'],
            ],
            'clinic'      => [
                'secondary_id' => $data['clinic']['id'],
                'name'         => $data['clinic']['name'],
            ],
            'patient'     => [
                'secondary_id'  => $data['patient']['id'],
                'clinic_id'     => $data['clinic']['id'],
                'name'          => $data['patient']['name'],
                'gender'        => $data['patient']['gender'],
                'date_of_birth' => Carbon::createFromFormat('Y-m-d', $data['patient']['dob']),
            ],
            'doctor'      => [
                'secondary_id' => $data['doctor']['id'],
                'name'         => $data['doctor']['name'],
                'clinic_id'    => $data['clinic']['id'],
            ],
            'specialty'   => [
                'secondary_id' => $data['specialty']['id'],
                'clinic_id'    => $data['clinic']['id'],
                'name'         => $data['specialty']['name'],
            ],
        ];
    }
}
