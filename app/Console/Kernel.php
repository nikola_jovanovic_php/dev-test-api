<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

/**
 * Class Kernel
 * @package App\Console
 */
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [//
    ];

    /**
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     *
     * @throws \Exception
     */
    protected function schedule(Schedule $schedule)
    {
        foreach (config('api_resources') as $resource) {
            switch ($resource['command']) {
                case 'auth:basic':
                    $command = sprintf(
                        "%s '%s' '%s' '%s' '%s' '%s'",
                        $resource['command'],
                        $resource['username'],
                        $resource['password'],
                        $resource['url'],
                        $resource['mapper'],
                        $resource['root_element']
                    );
                    break;
                case 'auth:jwt':
                    $command = sprintf(
                        "%s '%s' '%s' '%s' '%s' '%s' '%s'",
                        $resource['command'],
                        $resource['username'],
                        $resource['password'],
                        $resource['auth'],
                        $resource['url'],
                        $resource['mapper'],
                        $resource['root_element']
                    );
                    break;
                default:
                    throw new \Exception('Invalid command.');
            }
            $schedule->command($command)->daily();
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
