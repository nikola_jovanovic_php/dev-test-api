<?php

namespace App;

use Awobaz\Compoships\Database\Eloquent\Model;

/**
 * Class Specialty
 * @package App
 *
 * @property int                                                         $id
 * @property int                                                         $secondary_id
 * @property int                                                         $clinic_id
 * @property string                                                      $name
 * @property \App\Clinic                                                 $clinic
 * @property \Illuminate\Database\Eloquent\Collection|\App\Appointment[] $appointments
 */
class Specialty extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'clinic_id',
        'secondary_id',
    ];

    /**
     * @return \Awobaz\Compoships\Database\Eloquent\Relations\BelongsTo
     */
    public function clinic()
    {
        return $this->belongsTo(Clinic::class, 'secondary_id');
    }

    /**
     * @return \Awobaz\Compoships\Database\Eloquent\Relations\HasMany
     */
    public function appointments()
    {
        return $this->hasMany(Appointment::class, ['secondary_id', 'clinic_id']);
    }
}
