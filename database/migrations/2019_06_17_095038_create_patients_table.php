<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreatePatientsTable
 */
class CreatePatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'patients',
            function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('secondary_id');
                $table->unsignedBigInteger('clinic_id');
                $table->string('name');
                $table->date('date_of_birth');
                $table->string('gender');
                $table->timestamps();

                $table->unique(['secondary_id', 'clinic_id']);
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients');
    }
}
