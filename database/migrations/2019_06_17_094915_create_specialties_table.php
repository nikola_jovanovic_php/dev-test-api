<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateSpecialtiesTable
 */
class CreateSpecialtiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'specialties',
            function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('secondary_id');
                $table->unsignedBigInteger('clinic_id');
                $table->string('name');
                $table->timestamps();

                $table->unique(['secondary_id', 'clinic_id']);
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('specialties');
    }
}
