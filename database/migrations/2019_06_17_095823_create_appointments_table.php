<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateAppointmentsTable
 */
class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'appointments',
            function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('secondary_id');
                $table->unsignedBigInteger('clinic_id');
                $table->string('status');
                $table->dateTime('booked_at');
                $table->dateTime('datetime');
                $table->unsignedBigInteger('specialty_id');
                $table->unsignedBigInteger('doctor_id');
                $table->unsignedBigInteger('patient_id');
                $table->timestamps();

                $table->unique(['secondary_id', 'clinic_id']);
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
