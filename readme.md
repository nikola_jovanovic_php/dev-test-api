##### Clone the repo

```bash
git clone https://nikola_jovanovic_php@bitbucket.org/nikola_jovanovic_php/dev-test-api.git
cd dev-test-api
```

##### Copy .env.example to .env and update necessary vars 
 
```bash
cp .env.example .env
```

##### Install App dependencies

```bash
composer install
```

##### Generate encryption key for environment
 
```bash
php artisan key:generate
```

##### Migrate database 

```bash
php artisan migrate
```

##### Add the following Cron entry to your server

```bash
* * * * * cd /dev-test-api && php artisan schedule:run >> /dev/null 2>&1
```