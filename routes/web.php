<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get(
    '/',
    function (\Illuminate\Http\Request $request) {
        $validated    = $request->validate(
            [
                'doctor' => ['required_with:from', 'exists:doctors,id'],
                'from'   => ['required_with:doctor', 'date'],
            ]
        );
        $appointments = collect();
        if ($validated) {
            $appointments =
                \App\Appointment::notCancelled()->with('clinic', 'doctor', 'specialty', 'patient')->whereHas(
                    'doctor',
                    function (\Illuminate\Database\Eloquent\Builder $query) use ($validated) {
                        $query->where('id', '=', $validated['doctor']);
                    }
                )->whereDate('datetime', '=', \Carbon\Carbon::createFromFormat('m/d/Y', $validated['from']))->get();
        }
        $doctors = \App\Doctor::all();

        return view('welcome', compact('doctors', 'appointments'));
    }
);
