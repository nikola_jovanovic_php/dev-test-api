<?php

return [
    [
        'command'      => 'auth:basic',
        'username'     => 'Myah',
        'password'     => '654321',
        'url'          => 'http://ch-api-test.herokuapp.com/xml',
        'mapper'       => \App\Console\Commands\Clinic1Mapper::class,
        'root_element' => 'appointment',
    ],
    [
        'command'      => 'auth:jwt',
        'username'     => 'email:zlindgren@yahoo.com',
        'password'     => 'password:Tap!M3d1cal',
        'auth'         => 'http://ch-api-test.herokuapp.com/auth',
        'url'          => 'http://ch-api-test.herokuapp.com/json',
        'mapper'       => \App\Console\Commands\Clinic2Mapper::class,
        'root_element' => 'data',
    ],
];